process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0'

const db = require('./db.js');
const express = require('express');
const server = express();
const cors = require('cors');
const request = require('request');
const bodyParser = require('body-parser');
const moment = require('moment');

// session
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const sessionStore = session({
    secret: "lark-app",
    resave: true,
    saveUninitialized: false,
    cookie: {
        path: '/',
        expires: new Date(Date.now() + 7 * 24 * 60 * 60 * 1000),
        maxAge: 7 * 24 * 60 * 60 * 1000
    },
    store: new MongoStore({
        url: "mongodb://localhost:27017/larkapp"
    })
});

// server
server
    .set('view engine', 'pug')
    .use(cors())
    .use(bodyParser.urlencoded({
        extended: false,
        limit: "5mb"
    }))
    .use(bodyParser.json({
        limit: "5mb"
    }))
    .use(sessionStore)
    .get('/', home)
    .post('/repair/report', repairReport)
    .get('/repair/records', repairRecords)
    .use(express.static('static'))
    .listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", () => {
        console.log("Server listening at", [process.env.PWD, '/server.js'].join(''), process.env.IP || "0.0.0.0", process.env.PORT || 3000);
    });

// index.pug
function home(req, res) {
    res.render('index');
}

// repair - 校園巡管系統
function repairReport(req, res) {
    const record = {
        uuid: Date.now(),
        code: req.body.code,
        name: req.body.name,
        description: req.body.description,
        captureImage: req.body.captureImage,
        author: req.body.author,
        timestamp: Date.now()
    };

    new db.RepairRecord(record).save();

    res.send({ Message: 'Well done!' });
}

function repairRecords(req, res) {
    db.RepairRecord.find({}, (error, records) => {
        const items = [];
        records.forEach(r => {
            items.push({
                uuid: r.uuid,
                code: r.code,
                name: r.name,
                description: r.description,
                captureImage: r.captureImage,
                author: r.author,
                timestamp: moment(r.timestamp).format('YYYY/MM/DD HH:mm')
            });
        });

        res.send(items);
    });
}
