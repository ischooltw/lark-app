var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/larkapp');

var RepairRecord = mongoose.model('RepairRecord', {
	uuid: {
		type: String,
		index: {
			unique: true
		}
	},
	author: Object,
	code: String,
	name: String,
	description: String,
	captureImage: String,
	timestamp: Date
});

module.exports = {
	mongoose: mongoose,
	RepairRecord: RepairRecord
}
