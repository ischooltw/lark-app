var gulp = require('gulp');
var vulcanize = require('gulp-vulcanize');
var crisper = require('gulp-crisper');
var minifyInline = require('gulp-minify-inline');

gulp.task('default', function() { 
    return gulp.src('lark-app.html')
        .pipe(vulcanize({
            inlineScripts: true,
            inlineCss: true,
            stripComments: true
        }))
        // .pipe(minifyInline())
        .pipe(crisper())
        .pipe(gulp.dest('../dist'));
});
